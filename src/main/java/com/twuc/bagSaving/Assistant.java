package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class Assistant {

    private List<Cabinet> cabinets;

    private Cabinet actionCabinet;

    public Assistant(Cabinet... cabinets) {
        this.cabinets = Arrays.asList(cabinets);
    }

    private Cabinet getValidCabinet(Bag savedBag) {
        Optional<Cabinet> cabinetOptional = cabinets.stream().filter(
                cabinet -> cabinet.haveCapacity(savedBag.getBagSize())).findFirst();
        if (cabinetOptional.isPresent()) {
            return cabinetOptional.get();
        } else {
            throw new InsufficientLockersException("Insufficient empty lockers.");
        }
    }

    public Ticket saveBag(Bag savedBag) {
        actionCabinet = getValidCabinet(savedBag);
        switch (savedBag.getBagSize()) {
            case MINI:
            case SMALL:
                return actionCabinet.save(savedBag, LockerSize.SMALL);
            case MEDIUM:
                return actionCabinet.save(savedBag, LockerSize.MEDIUM);
            case BIG:
                return actionCabinet.save(savedBag, LockerSize.BIG);
        }
        throw new IllegalArgumentException("bag size not valid");
    }

    public Bag getBag(Ticket ticket) {
        AtomicReference<Bag> bagAtomic = new AtomicReference<>();
        cabinets.forEach(cabinet -> {
            Bag fetchedBag = null;
            try {
                fetchedBag = cabinet.getBag(ticket);
            } catch (IllegalArgumentException ignore) {
            }
            if (fetchedBag != null) {
                bagAtomic.set(fetchedBag);
            }
        });
        if (bagAtomic.get() == null) {
            throw new IllegalArgumentException("Invalid ticket.");
        }
        return bagAtomic.get();
    }
}
