package com.twuc.bagSaving;

@SuppressWarnings("WeakerAccess") // will expose to public
public class LockerSetting {
    private final LockerSize lockerSize;
    private final Integer capacity;

    public LockerSetting(LockerSize lockerSize, Integer capacity) {
        this.lockerSize = lockerSize;
        this.capacity = capacity;
    }

    public static LockerSetting of(LockerSize lockerSize, Integer capacity) {
        return new LockerSetting(lockerSize, capacity);
    }

    public LockerSize getLockerSize() {
        return lockerSize;
    }

    public Integer getCapacity() {
        return capacity;
    }
}
