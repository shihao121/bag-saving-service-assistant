package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import static com.twuc.bagSaving.CabinetFactory.*;
import static org.junit.jupiter.api.Assertions.*;

class SaveAndGetBagTest extends BagSavingArgument {
    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_a_ticket_when_saving_a_bag(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Ticket ticket = cabinet.save(new Bag(bagSize), lockerSize);
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_save_and_get_bag_when_locker_is_empty(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = cabinet.save(savedBag, lockerSize);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_smaller_bag_to_bigger_locker(
        BagSize smallerBagSize, LockerSize biggerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag smallerBag = new Bag(smallerBagSize);
        Ticket ticket = cabinet.save(smallerBag, biggerLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(smallerBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createNonSavableBagSizeAndLockerSize")
    void should_throw_when_saving_bigger_bag_to_smaller_locker(BagSize biggerBagSize, LockerSize smallerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        IllegalArgumentException exception =
            assertThrows(IllegalArgumentException.class,
                () -> cabinet.save(new Bag(biggerBagSize), smallerLockerSize));

        assertEquals(
            String.format("Cannot save %s bag to %s locker.", biggerBagSize, smallerLockerSize),
            exception.getMessage());
    }

    @Test
    void should_throw_if_locker_size_is_not_specified() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.save(new Bag(BagSize.BIG), null));
    }

    @Test
    void should_throw_if_no_ticket_is_provided() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        final IllegalArgumentException error = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(null));
        assertEquals("Please use your ticket.", error.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_not_provided_by_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Ticket ticket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_used(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(bagSize);
        Ticket ticket = cabinet.save(bag, lockerSize);
        cabinet.getBag(ticket);

        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_generated_by_another_cabinet(BagSize bagSize, LockerSize lockerSize) {
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Ticket generatedByAnotherCabinet = anotherCabinet.save(new Bag(bagSize), lockerSize);

        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.getBag(generatedByAnotherCabinet));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
    void should_throw_if_correspond_lockers_are_full(
        Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        InsufficientLockersException error = assertThrows(
            InsufficientLockersException.class,
            () -> fullCabinet.save(savedBag, lockerSize));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @ParameterizedTest
    @EnumSource(value = LockerSize.class, mode = EnumSource.Mode.EXCLUDE)
    void should_throw_when_saving_nothing(LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        final IllegalArgumentException exception = assertThrows(
            IllegalArgumentException.class,
            () -> cabinet.save(null, lockerSize));

        assertEquals("Please at least put something here.", exception.getMessage());
    }

    @Test
    void should_get_ticket_when_assistant_save_bag() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        Bag savedBag = new Bag(BagSize.BIG);
        Ticket ticket = assistant.saveBag(savedBag);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @Test
    void should_get_bag_when_assistant_get_bag() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag saveBag = new Bag(BagSize.BIG);
        Ticket ticket = cabinet.save(saveBag, LockerSize.BIG);
        Assistant assistant = new Assistant(cabinet);
        Bag bag = assistant.getBag(ticket);
        assertSame(saveBag, bag);
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"BIG", "MEDIUM", "SMALL"})
    void should_save_bag_access(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        Ticket ticket = assistant.saveBag(new Bag(bagSize));
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"BIG", "MEDIUM", "SMALL"})
    void should_fail_if_cabinet_have_no_capacity(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithOneCapacity();
        Assistant assistant = new Assistant(cabinet);
        assistant.saveBag(new Bag(bagSize));
        assertThrows(InsufficientLockersException.class, () -> assistant.saveBag(new Bag(bagSize)));
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"BIG", "MEDIUM", "SMALL"})
    void should_save_bag_to_two_cabinet(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithOneCapacity();
        Cabinet anotherCabinet = createCabinetWithOneCapacity();
        Assistant assistant = new Assistant(cabinet, anotherCabinet);
        Bag firstBag = new Bag(bagSize);
        Bag secondBag = new Bag(bagSize);
        Ticket firstTicket = assistant.saveBag(firstBag);
        Ticket secondTicket = assistant.saveBag(secondBag);
        Bag fetchedFirstBag = cabinet.getBag(firstTicket);
        Bag fetchedSecondBag = anotherCabinet.getBag(secondTicket);
        assertSame(firstBag, fetchedFirstBag);
        assertSame(secondBag, fetchedSecondBag);
    }

    @Test
    void should_save_bag_to_specify_locker() {
        Cabinet smallCabinet = createCabinetWithSpecifyLockerSize(LockerSize.SMALL, 1);
        Cabinet mediumCabinet = createCabinetWithSpecifyLockerSize(LockerSize.MEDIUM, 1);
        Assistant assistant = new Assistant(smallCabinet, mediumCabinet);
        Ticket ticket = assistant.saveBag(new Bag(BagSize.MEDIUM));
        Ticket anotherTicket = assistant.saveBag(new Bag(BagSize.SMALL));
        Bag fetchedSmallBag = mediumCabinet.getBag(ticket);
        Bag fetchedMediumBag = smallCabinet.getBag(anotherTicket);
        assertNotNull(fetchedMediumBag);
        assertNotNull(fetchedSmallBag);
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"BIG", "MEDIUM", "SMALL"})
    void should_get_specify_bag_from_two_cabinet(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithOneCapacity();
        Cabinet anotherCabinet = createCabinetWithOneCapacity();
        Assistant assistant = new Assistant(cabinet, anotherCabinet);
        Bag firstBag = new Bag(bagSize);
        Bag secondBag = new Bag(bagSize);
        Ticket firstTicket = assistant.saveBag(firstBag);
        Ticket secondTicket = assistant.saveBag(secondBag);
        Bag fetchedFirstBag = assistant.getBag(firstTicket);
        Bag fetchedSecondBag = assistant.getBag(secondTicket);
        assertSame(firstBag, fetchedFirstBag);
        assertSame(secondBag, fetchedSecondBag);
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"BIG", "MEDIUM", "SMALL"})
    void should_get_specify_bag_by_another_assistant(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithOneCapacity();
        Cabinet anotherCabinet = createCabinetWithOneCapacity();
        Assistant assistant = new Assistant(cabinet, anotherCabinet);
        Assistant anotherAssistant = new Assistant(cabinet, anotherCabinet);
        Bag firstBag = new Bag(bagSize);
        Bag secondBag = new Bag(bagSize);
        Ticket firstTicket = assistant.saveBag(firstBag);
        Ticket secondTicket = assistant.saveBag(secondBag);
        Bag fetchedFirstBag = anotherAssistant.getBag(firstTicket);
        Bag fetchedSecondBag = anotherAssistant.getBag(secondTicket);
        assertSame(firstBag, fetchedFirstBag);
        assertSame(secondBag, fetchedSecondBag);
    }
}
